Yii PHP Framework Version 2 / NOX Widgets
=========================================

Collection of useful widgets for Yii Framework 2.0.

## Installation

The preferred way to install this extension is through [composer](http://composer.org). Either run:

```
php composer.phar require --prefer-dist nox-it/yii2-nox-widgets "*"
```

or add

```
"nox-it/yii2-nox-widgets": "*"
```

to the require section of your composer.json file.

![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)
