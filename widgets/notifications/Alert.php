<?php

    namespace nox\widgets\notifications;

    /**
     * Extends the \yii\bootstrap\Alert widget with additional styling and auto fade out options.
     *
     * @since  1.0
     */
    class Alert extends \kartik\alert\Alert
    {
    }
