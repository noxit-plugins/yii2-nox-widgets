<?php

    namespace nox\widgets\notifications;

    /**
     * Widget that wraps the Bootstrap Growl plugin by remabledesigns.
     *
     * @http   ://bootstrap-growl.remabledesigns.com/
     *
     * @since  1.0
     */
    class Growl extends \kartik\growl\Growl
    {
    }
