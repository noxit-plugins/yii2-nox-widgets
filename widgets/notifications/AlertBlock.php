<?php

    namespace nox\widgets\notifications;

    /**
     * Alert block widget that groups multiple `\kartik\widget\Alert` or `kartik\widget\Growl` widgets as one single
     * block. You can choose to automatically read and display session flash messages (which is the default setting) or
     * setup your own block of custom alerts.
     *
     * @since  1.0
     */
    class AlertBlock extends \kartik\alert\AlertBlock
    {
    }
