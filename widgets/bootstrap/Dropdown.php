<?php

    namespace nox\widgets\bootstrap;

    use kartik\dropdown\DropdownX;

    /**
     * An extended dropdown menu for Bootstrap 3 - that offers
     * submenu drilldown
     *
     * @author Kartik Visweswaran <kartikv2@gmail.com>
     * @since  1.0
     * @see    http://demos.krajee.com/dropdown-x
     */
    class Dropdown extends DropdownX
    {
        const YES = 1;
        const NO = 0;

        public $encodeLabels = false;

        /**
         * @param int    $status
         * @param string $label
         *
         * @return string
         */
        public static function getHandIconByStatus($status = self::NO, $label = '')
        {
            $status = (int)$status;

            if ($status === self::YES) {
                return '<span class="glyphicon glyphicon-thumbs-up text-success" aria-hidden="true"></span> ' . $label;
            } else {
                return '<span class="glyphicon glyphicon-thumbs-down text-danger" aria-hidden="true"></span> ' . $label;
            }
        }

        /**
         * @param string $label
         * @param string $icon
         * @param string $classes
         *
         * @return string
         */
        public static function getGlyphicon($label = '', $icon, $classes = '')
        {
            return "<span class=\"{$classes}\"><span class=\"glyphicon glyphicon-{$icon}\" aria-hidden=\"true\"></span> {$label}</span>";
        }
    }
