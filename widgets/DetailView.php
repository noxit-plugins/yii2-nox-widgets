<?php

    namespace nox\widgets;

    use Yii;

    /**
     * Class DetailView
     *
     * @category Widgets
     * @author   Jonatas Sas
     *
     * @package  nox\widgets
     */
    class DetailView extends \yii\widgets\DetailView
    {
        /**
         * @inheritdoc
         */
        public $template = '<tr><th style="width:200px;text-align:right;">{label}</th><td>{value}</td></tr>';

        /**
         * @inheritdoc
         */
        public $options = ['class' => 'table table-striped table-bordered detail-view'];
    }
