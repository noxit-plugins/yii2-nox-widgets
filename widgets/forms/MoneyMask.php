<?php

    namespace nox\widgets\forms;

    use kartik\money\MaskMoney;
    use Yii;

    /**
     * A money mask input widget styled for Bootstrap 3 based on the jQuery-maskMoney plugin,
     * which offers a simple way to create masks to your currency form fields.
     *
     * @see http://demos.krajee.com/money
     */
    class MoneyMask extends MaskMoney
    {
        public static $defaultParams = [
            'simple' => [
                'prefix'        => '',
                'suffix'        => '',
                'affixesStay'   => true,
                'thousands'     => '.',
                'decimal'       => ',',
                'precision'     => 2,
                'allowZero'     => true,
                'allowNegative' => false
            ]
        ];
    }
