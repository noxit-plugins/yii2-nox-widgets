<?php

    namespace nox\widgets\forms;

    /**
     * TouchSpin widget is a Yii2 wrapper for the bootstrap-touchspin plugin by
     * István Ujj-Mészáros. This input widget is a mobile and touch friendly input
     * spinner component for Bootstrap 3.
     *
     * @since  1.0
     * @see    http://www.virtuosoft.eu/code/bootstrap-touchspin/
     */
    class TouchSpin extends \kartik\touchspin\TouchSpin
    {
    }
