<?php

    namespace nox\widgets\forms;

    use common\components\helpers\ArrayHelper;
    use kartik\date\DatePickerAsset;
    use kartik\field\FieldRangeAsset;

    /**
     * DatePicker widget is a Yii2 wrapper for the Bootstrap DatePicker plugin. The
     * plugin is a fork of Stefan Petre's DatePicker (of eyecon.ro), with improvements
     * by @eternicode.
     *
     * @since  1.0
     * @see    http://eternicode.github.io/bootstrap-datepicker/
     */
    class DatePicker extends \kartik\date\DatePicker
    {
        /**
         * Registers the needed client assets
         */
        public function registerAssets()
        {
            if ($this->disabled) {
                return;
            }

            $view = $this->getView();

            if (!empty($this->_langFile)) {
                DatePickerAsset::register($view)->js[] = $this->_langFile;
            } else {
                DatePickerAsset::register($view);
            }

            $id = "jQuery('#" . $this->options['id'] . "')";

            $this->options['data-datepicker-type'] = $this->type;

            if ($this->type == self::TYPE_INLINE) {
                if (!isset($this->pluginEvents['changeDate'])) {
                    $this->pluginEvents = ArrayHelper::merge($this->pluginEvents, ['changeDate' => 'function (e) { ' . $id . '.val(e.format());} ']);
                }
            }

            if ($this->type === self::TYPE_INPUT) {
                $this->registerPlugin('kvDatepicker');
            } elseif ($this->type === self::TYPE_RANGE && isset($this->form)) {
                $this->registerPlugin('kvDatepicker', "{$id}.parent().parent()");
            } else {
                $this->registerPlugin('kvDatepicker', "{$id}.parent()");
            }

            if ($this->removeButton !== false && $this->_hasAddon) {
                $view->registerJs("{$id}.parent().find('.kv-date-remove').on('click', function() {{$id}.parent().datepicker('clearDates');});");
            }

            if ($this->type === self::TYPE_RANGE) {
                FieldRangeAsset::register($view);
            }
        }
    }
