<?php

    namespace nox\widgets\forms;

    /**
     * A widget that wraps the spin.js - an animated CSS3 loading spinner
     * with VML fallback for IE.
     */
    class Spinner extends \kartik\spinner\Spinner
    {
    }
