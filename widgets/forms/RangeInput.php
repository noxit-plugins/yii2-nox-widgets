<?php

    namespace nox\widgets\forms;

    use Yii;

    /**
     * RangeInput widget is an enhanced widget encapsulating the HTML 5 range input.
     *
     * @since  1.0
     * @see    http://twitter.github.com/typeahead.js/examples
     */
    class RangeInput extends \kartik\range\RangeInput
    {
    }
