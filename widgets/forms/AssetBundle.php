<?php

    namespace nox\widgets\forms;

    /**
     * Base asset bundle for all widgets
     *
     * @since  1.0
     */
    class AssetBundle extends \kartik\base\AssetBundle
    {
    }
