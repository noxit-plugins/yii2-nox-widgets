<?php

    namespace nox\widgets\forms;

    /**
     * ColorInput widget is an enhanced widget encapsulating the HTML 5 color input.
     *
     * @since  1.0
     * @see    http://twitter.github.com/typeahead.js/examples
     */
    class ColorInput extends \kartik\color\ColorInput
    {
    }
