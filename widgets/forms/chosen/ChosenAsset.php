<?php

    namespace nox\widgets\forms\chosen;

    use yii\web\AssetBundle;

    /**
     * ChosenAsset
     */
    class ChosenAsset extends AssetBundle
    {
        /**
         * @inheritdoc
         */
        public $sourcePath = '@nox/widgets/form/chosen/assets';

        /**
         * @inheritdoc
         */
        public $css = [
            'css/chosen.bootstrap.css'
        ];

        /**
         * @inheritdoc
         */
        public $js = [
            'js/chosen.jquery.js'
        ];

        /**
         * @inheritdoc
         */
        public $depends = [
            'yii\bootstrap\BootstrapAsset',
            'yii\web\JqueryAsset',
        ];

        /**
         * @inheritdoc
         */
        public function init()
        {
            parent::init();
        }
    }
