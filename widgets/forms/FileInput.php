<?php

    namespace nox\widgets\forms;

    use Yii;
    use nox\helpers\HtmlHelper;

    /**
     * FileInput widget styled for Bootstrap 3.0 with ability to multiple file
     * selection and preview, format button styles and inputs. Runs on all modern
     * browsers supporting HTML5 File Inputs and File Processing API. For browser
     * versions IE9 and below, this widget will gracefully degrade to normal HTML
     * file input.
     *
     * Wrapper for the Bootstrap FileInput JQuery Plugin by Krajee
     *
     * @see    http://plugins.krajee.com/bootstrap-fileinput
     * @see    https://github.com/kartik-v/bootstrap-fileinput
     *
     * @since  2.0
     * @see    http://twitter.github.com/typeahead.js/examples
     * @see    http://demos.krajee.com/widget-details/fileinput
     */
    class FileInput extends \kartik\file\FileInput
    {
        /**
         * @var array
         */
        public static $defaultConfigWithPreview = [
            'options'       => [
                'accept' => 'text/plain, text/csv, application/pdf, application/x-pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/msword, application/rtf, text/richtext, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.oasis.opendocument.text, image/gif, image/jpeg, image/png',
                'multiple' => false
            ],
            'pluginOptions' => [
                'browseLabel' => 'Procurar...',
                'showPreview' => true,
                'showCaption' => true,
                'showRemove'  => false,
                'showUpload'  => false
            ]
        ];

        /**
         * @var array
         */
        public static $noCaptionConfigWithPreview = [
            'options'       => [
                'accept' => 'image/gif, image/jpeg, image/png',
                'multiple' => false
            ],
            'pluginOptions' => [
                'browseLabel' => 'Procurar...',
                'showPreview' => true,
                'showCaption' => false,
                'showRemove'  => false,
                'showUpload'  => false
            ]
        ];

        /**
         * @param string $caption
         * @param string $file
         * @param bool   $noCaption
         *
         * @return array
         */
        public static function getDefaultConfigWithPreview($caption = '', $file = '', $noCaption = false)
        {
            $defaultConfigWithPreview = (((bool)$noCaption) ? self::$noCaptionConfigWithPreview : self::$defaultConfigWithPreview);

            if (!empty($file)) {
                $defaultConfigWithPreview['pluginOptions']['initialPreview'] = [
                    HtmlHelper::img($file, [
                        'class' => 'file-preview-image',
                        'alt'   => $caption,
                        'title' => $caption
                    ])
                ];
                $defaultConfigWithPreview['pluginOptions']['initialCaption'] = $caption;
                $defaultConfigWithPreview['pluginOptions']['overwriteInitial'] = false;
            }

            return $defaultConfigWithPreview;
        }
    }
