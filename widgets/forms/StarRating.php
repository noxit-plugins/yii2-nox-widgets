<?php

    namespace nox\widgets\forms;

    /**
     * StarRating widget is a wrapper widget for the Bootstrap Star Rating plugin by Krajee.
     * This plugin is a simple star rating yet powerful control that converts a  'number' input
     * to a star rating control using JQuery. The widget is styled for Bootstrap 3.0.
     *
     * Upgraded for the new plugin support. Includes fractional ratings with editable star
     * symbol, RTL inputs, and custom styling.
     *
     * @see    http://plugins.krajee.com/star-rating
     * @see    http://github.com/kartik-v/bootstrap-star-rating
     * @since  1.0
     */
    class StarRating extends \kartik\rating\StarRating
    {
    }
