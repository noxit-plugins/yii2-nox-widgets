<?php

    namespace nox\widgets\forms;

    /**
     * Dependent Dropdown widget is a wrapper widget for the dependent-dropdown
     * JQuery plugin by Krajee. The plugin enables setting up dependent dropdowns
     * with nested dependencies.
     *
     * @see    http://plugins.krajee.com/dependent-dropdown
     * @see    http://github.com/kartik-v/dependent-dropdown
     * @since  1.0.0
     */
    class DepDrop extends \kartik\depdrop\DepDrop
    {
    }
