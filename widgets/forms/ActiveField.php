<?php

    namespace nox\widgets\forms;

    /**
     * Extends the ActiveField widget to handle various bootstrap form types and handle input groups.
     *
     * Example(s):
     * ```php
     *    echo $this->form->field($model, 'email', ['addon' => ['type'=>'prepend', 'content'=>'@']]);
     *    echo $this->form->field($model, 'amount_paid', ['addon' => ['type'=>'append', 'content'=>'.00']]);
     *    echo $this->form->field($model, 'phone', ['addon' => ['type'=>'prepend', 'content'=>'<i class="glyphicon
     *     glyphicon-phone']]);
     * ```
     *
     * @author Kartik Visweswaran <kartikv2@gmail.com>
     * @since  1.0
     *
     * @see    http://demos.krajee.com/widget-details/active-field
     */
    class ActiveField extends \kartik\form\ActiveField
    {
    }
