<?php

    namespace nox\widgets\forms;

    use Yii;

    /**
     * MaskedInput generates a masked text input.
     *
     * MaskedInput is similar to [[HtmlHelper::textInput()]] except that an input mask will be used to force users to enter
     * properly formatted data, such as phone numbers, social security numbers.
     *
     * To use MaskedInput, you must set the [[mask]] property. The following example
     * shows how to use MaskedInput to collect phone numbers:
     *
     * ```php
     * echo MaskedInput::widget([
     *     'name' => 'phone',
     *     'mask' => '999-999-9999',
     * ]);
     * ```
     *
     * You can also use this widget in an [[yii\widgets\ActiveForm|ActiveForm]] using the [[yii\widgets\ActiveField::widget()|widget()]]
     * method, for example like this:
     *
     * ```php
     * <?= $form->field($model, 'from_date')->widget(\yii\widgets\MaskedInput::className(), [
     *     'mask' => '999-999-9999',
     * ]) ?>
     * ```
     *
     * @see http://demos.krajee.com/masked-input
     */
    class MaskedInput extends \yii\widgets\MaskedInput
    {
        /**
         * @var array
         */
        protected static $masks = [
            'completePhone' => '+99 (99) 9{4,5}-9{4}',
            'phone'         => '(99) 9{4}-9{4}',
            'mobilePhone'   => '(99) 9{4}-9{4,5}',
            'zipcode'       => '99999-999',
        ];

        /**
         * @param string $id
         *
         * @return string
         */
        public static function getPredefinedMask($id)
        {
            if (isset(static::$masks[$id])) {
                return static::$masks[$id];
            }

            return '';
        }
    }
