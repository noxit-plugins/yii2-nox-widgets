<?php

    namespace nox\widgets\forms;

    /**
     * Switch widget is a Yii2 wrapper for the Bootstrap Switch plugin by Mattia, Peter, & Emanuele.
     * This input widget is a jQuery based replacement for checkboxes and radio buttons and converts
     * them to toggle switches.
     *
     * @since  1.0
     * @see    http://www.bootstrap-switch.org/
     */
    class SwitchInput extends \kartik\switchinput\SwitchInput
    {
    }
