<?php

    namespace nox\widgets\forms\gmaps;

    use yii\web\AssetBundle;
    use yii\web\View;

    /**
     * Class GmapInputWidgetAsset
     *
     * @category Asset
     * @author   Jonatas Sas
     *
     * @package  nox\widgets\forms\gmaps
     */
    class GmapInputWidgetAsset extends AssetBundle
    {
        /**
         * @inheritdoc
         */
        public $sourcePath = null;

        /**
         * @inheritdoc
         */
        public $css = [];

        /**
         * @inheritdoc
         */
        public $js = ['https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places'];

        /**
         * @inheritdoc
         */
        public $jsOptions = ['position' => View::POS_HEAD];

        /**
         * @inheritdoc
         */
        public $depends = ['yii\web\JqueryAsset'];

        /**
         * @inheritdoc
         */
        public function init()
        {
            parent::init();
        }
    }
