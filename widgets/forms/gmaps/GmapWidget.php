<?php

    namespace nox\widgets\forms\gmaps;

    use Yii;
    use yii\base\Widget;

    /**
     * Class GmapInputWidget
     *
     * @category Widget
     * @author   Jonatas Sas
     *
     * @package  nox\widgets\forms\gmaps
     */
    class GmapWidget extends Widget
    {
        const DEFAULT_LATITUDE = -23.5505199;
        const DEFAULT_LONGITUDE = -46.63330939999997;

        /**
         * @inheritdoc
         */
        public $name = 'gmap';

        /**
         * @var integer
         */
        public $mapId;

        /**
         * @var string
         */
        public $searchPlaceholder = 'Digite uma Localização...';

        /**
         * @var float
         */
        public $latitude = self::DEFAULT_LATITUDE;

        /**
         * @var float
         */
        public $longitude = self::DEFAULT_LONGITUDE;

        /**
         * @var integer
         */
        public $mapHeight = 400;

        /**
         * @var integer
         */
        public $mapMarginBottom = 25;

        /**
         * @inheritdoc
         */
        public function init()
        {
            parent::init();

            $this->mapId = date('YmdHis') . rand(100000, 999999);

            $view = $this->getView();

            if (empty($this->latitude)) {
                $this->latitude = self::DEFAULT_LATITUDE;
            }

            if (empty($this->longitude)) {
                $this->longitude = self::DEFAULT_LONGITUDE;
            }

            GmapInputWidgetAsset::register($view);
        }

        /**
         * @inheritdoc
         */
        public function run()
        {
            echo $this->getStyle();
            echo $this->getScript();
            echo $this->getHtml();
        }

        /**
         * @return string
         */
        private function getStyle()
        {
            $id = $this->id;
            $mapId = $this->mapId;
            $mapHeight = (int)$this->mapHeight;

            return <<<STYLES
<style type="text/css">
    div#gmap-{$id}-{$mapId}-canvas{height:{$mapHeight}px;margin:0;padding:0}
</style>
STYLES;
        }

        /**
         * @return string
         */
        private function getScript()
        {
            $id = $this->id;
            $mapId = $this->mapId;
            $latitude = $this->latitude;
            $longitude = $this->longitude;

            return <<<SCRIPT
<script type="text/javascript">
    function initializeGmap_{$id}_{$mapId}() {
        var elements = {
            map:       'gmap-{$id}-{$mapId}-canvas',
            search:    'gmap-{$id}-{$mapId}-search-input'
        };

        var mapOptions = {
            zoom:              18,
            panControl:        false,
            zoomControl:       true,
            scaleControl:      false,
            streetViewControl: false,
            scrollwheel:       false,
            center:            new google.maps.LatLng({$latitude}, {$longitude}),
            mapTypeId:         google.maps.MapTypeId.ROADMAP
        }

        var map     = new google.maps.Map(document.getElementById(elements.map), mapOptions);
        var markers = [];
        var input   = (document.getElementById(elements.search));

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        marker = new google.maps.Marker({map: map, title: '', position: new google.maps.LatLng({$latitude}, {$longitude})});

        markers.push(marker);
    }

    google.maps.event.addDomListener(window, 'load', initializeGmap_{$id}_{$mapId});
</script>
SCRIPT;
        }

        /**
         * @return string
         */
        private function getHtml()
        {
            $id = $this->id;
            $mapId = $this->mapId;

            return <<<HTML
<div id="gmap-{$id}-{$mapId}-canvas"></div>
HTML;
        }
    }
