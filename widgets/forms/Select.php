<?php

    namespace nox\widgets\forms;

    use kartik\select2\Select2;

    /**
     * Select2 widget is a Yii2 wrapper for the Select2 jQuery plugin. This
     * input widget is a jQuery based replacement for select boxes. It supports
     * searching, remote data sets, and infinite scrolling of results. The widget
     * is specially styled for Bootstrap 3.
     *
     * @since  1.0
     * @see    http://ivaynberg.github.com/select2/
     * @see    http://demos.krajee.com/widget-details/select2
     */
    class Select extends Select2
    {
    }
