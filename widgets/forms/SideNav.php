<?php

    namespace nox\widgets\forms;

    /**
     * A custom extended side navigation menu extending Yii Menu
     *
     * For example:
     *
     * ```php
     * echo SideNav::widget([
     *     'items' => [
     *         [
     *             'url' => ['/site/index'],
     *             'label' => 'Home',
     *             'icon' => 'home'
     *         ],
     *         [
     *             'url' => ['/site/about'],
     *             'label' => 'About',
     *             'icon' => 'info-sign',
     *             'items' => [
     *                  ['url' => '#', 'label' => 'Item 1'],
     *                  ['url' => '#', 'label' => 'Item 2'],
     *             ],
     *         ],
     *     ],
     * ]);
     * ```
     */
    class SideNav extends \kartik\sidenav\SideNav
    {
    }
