<?php

    namespace nox\widgets\forms;

    /**
     * Extends the ActiveForm widget to handle various
     * bootstrap form types.
     *
     * Example(s):
     * ```php
     * // Horizontal Form
     * $form = ActiveForm::begin([
     *      'id' => 'form-signup',
     *      'type' => ActiveForm::TYPE_HORIZONTAL
     * ]);
     * // Inline Form
     * $form = ActiveForm::begin([
     *      'id' => 'form-login',
     *      'type' => ActiveForm::TYPE_INLINE
     *      'fieldConfig' => ['autoPlaceholder'=>true]
     * ]);
     * // Horizontal Form Configuration
     * $form = ActiveForm::begin([
     *      'id' => 'form-signup',
     *      'type' => ActiveForm::TYPE_HORIZONTAL
     *      'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL]
     * ]);
     *
     * @since  1.0
     *
     * @see    http://demos.krajee.com/widget-details/active-field
     */
    class ActiveForm extends \kartik\form\ActiveForm
    {
        public function init()
        {
            if (!isset($this->fieldConfig['class'])) {
                $this->fieldConfig['class'] = ActiveField::className();
            }

            parent::init();

            $validationScript = <<<SCRIPT
$('#{$this->id}').on(
    'afterValidate',
    function (event, messages, errorAttributes) {
        if (errorAttributes.length > 0) {
            if (errorAttributes[0].id) {
                $('html, body').animate({scrollTop: ($('#' + errorAttributes[0].id).offset().top - 30)}, 'slow');
            }
        }

        return true;
    }
);
SCRIPT;

            $this->view->registerJs($validationScript);
        }
    }
