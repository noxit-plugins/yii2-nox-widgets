<?php

    namespace nox\widgets\forms\upload;

    /**
     * FileUploadUI
     *
     * Widget to render the jQuery File Upload UI plugin as shown in
     * [its demo](http://blueimp.github.io/jQuery-File-Upload/index.html)
     */
    class FileUploadUI extends \dosamigos\fileupload\FileUploadUI
    {

    }
