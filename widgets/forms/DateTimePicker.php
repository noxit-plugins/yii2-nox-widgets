<?php

    namespace nox\widgets\forms;

    /**
     * DateTimePicker widget is a Yii2 wrapper for the Bootstrap DateTimePicker plugin by smalot
     * This is a fork of the DatePicker plugin by @eternicode and adds the time functionality.
     *
     * @since  1.0
     * @see    http://www.malot.fr/bootstrap-datetimepicker/
     */
    class DateTimePicker extends \kartik\datetime\DateTimePicker
    {
    }
