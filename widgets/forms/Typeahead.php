<?php

    namespace nox\widgets\forms;

    /**
     * Typeahead widget is a Yii2 wrapper for the Twitter typeahead.js plugin. This
     * input widget is a jQuery based replacement for text inputs providing search
     * and typeahead functionality. It is inspired by twitter.com's autocomplete search
     * functionality and based on Twitter's typeahead.js which Twitter mentions as
     * a fast and fully-featured autocomplete library.
     *
     * This is an advanced implementation of the typeahead.js plugin included with the
     * Bloodhound suggestion engine.
     *
     * @since  1.0
     * @see    http://twitter.github.com/typeahead.js/examples
     */
    class Typeahead extends \kartik\typeahead\Typeahead
    {
    }
