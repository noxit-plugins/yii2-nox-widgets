<?php

    namespace nox\widgets\forms;

    /**
     * Base input widget class for yii2-widgets
     *
     * @since  1.0
     */
    class InputWidget extends \kartik\base\InputWidget
    {
    }
