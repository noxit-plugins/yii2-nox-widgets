<?php

    namespace nox\widgets\forms;

    use Yii;

    /**
     * Common base widget asset bundle for all Krajee widgets
     *
     * @since  1.0
     */
    class WidgetAsset extends \kartik\base\WidgetAsset
    {
    }
