<?php

    namespace nox\widgets\forms;

    /**
     *
     * TinyMCE renders a tinyMCE js plugin for WYSIWYG editing.
     *
     * @link https://github.com/2amigos/yii2-tinymce-widget
     */
    class TinyMce extends \dosamigos\tinymce\TinyMce
    {
        public static $defaultClientOptions = [
            'simple' => [
                'plugins' => [
                    'advlist autolink lists link charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste'
                ],
                'toolbar' => 'undo redo | bold italic | link'
            ]
        ];
    }
